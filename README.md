# Bot Roule Dédé

* Utilise la commande /r , /d ou /roll pour rouler
> /r 5d6+5

* Gère les lancés en avantages ou désavantages
> /r 1d20a   
> /r 1d20d

* Pour inviter ce bot vas sur :   
https://discord.com/api/oauth2/authorize?client_id=834148552078524517&permissions=67584&scope=bot